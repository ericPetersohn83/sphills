define('q2ngam/themejs/theme-q2',["exports"], function(__exports__) {
var themeJS = {
	/*Auto Created by Curly WM 2.0.7.1*/
	themeName: "theme-q2",
	cssName: "q2",
	lpColorMap: {
		"C": "#7d9638",
		"S": "#d99741",
		"L": "#dd8862",
		"X": "#a53326",
		"defaultColor": "#a53326"
	},
	widgets: [{
			page: "dashboard",
			location: "bottom",
			orientation: "horizontal",
			devices: ["tablet", "desktop"],
			widgets: [{
					name: "AccountSummary"
				}
			]
		}, {
			page: "dashboard",
			location: "top",
			orientation: "horizontal",
			devices: ["desktop", "tablet"],
			widgets: [{
					name: "FullBannerAd"
				},
				{
				   name: 'DynamicForm',
				   devices: ['phone','tablet'],
				   extras: {
								  formId: 26,
								  selfSubmitting: true
				   }
				}
			]
		}, {
			page: "dashboard",
			location: "right",
			orientation: "vertical",
			widgets: [{
					name: "Billpay",
					vendorId: 6
				}, {
					name: "MediumAd"
				}, {
					name: "RDC"
				}, {
					name: "Approvals",
					titleLoc: "_t.approvals_widget.title",
					buttonLoc: "_t.approvals_widget.action",
					alwaysOn: true
				}
			]
		}, {
			page: "commercial",
			location: "right",
			orientation: "vertical",
			widgets: [{
					name: "MediumAd",
					gravity: "above"
				}
			]
		}, {
			"page": "commercial",
			"location": "top",
			"orientation": "horizontal",
			"devices": ["tablet", "desktop"],
			"widgets": [{
					"name": "FullBannerAd"
				}
			]
		}, {
			page: "template",
			location: "right",
			orientation: "vertical",
			widgets: [{
					name: "MediumAd",
					gravity: "above"
				}
			]
		}, {
			page: "recipient",
			location: "right",
			orientation: "vertical",
			widgets: [{
					name: "MediumAd",
					gravity: "above"
				}
			]
		}, {
			page: "transfer",
			location: "right",
			orientation: "vertical",
			widgets: [{
					name: "MediumAd",
					gravity: "above"
				}
			]
		}, {
			page: "billpay",
			location: "right",
			orientation: "vertical",
			widgets: [{
					name: "MediumAd",
					gravity: "above"
				}
			]
		}, {
			page: "rdc",
			location: "right",
			orientation: "vertical",
			widgets: [{
					name: "MediumAd",
					gravity: "above"
				}
			]
		}, {
			page: "news",
			location: "right",
			orientation: "vertical",
			widgets: [{
					name: "MediumAd",
					gravity: "above"
				}, {
					name: "Rates",
					gravity: "above"
				}
			]
		}, {
			page: "branches",
			location: "right",
			orientation: "vertical",
			widgets: [{
					name: "MediumAd",
					gravity: "above"
				}
			]
		}, {
			page: "messages",
			location: "top",
			orientation: "horizontal",
			devices: ["tablet", "desktop"],
			widgets: [{
					name: "BannerAd"
				}
			]
		}, {
			page: "messages",
			location: "right",
			orientation: "vertical",
			widgets: [{
					name: "MediumAd",
					gravity: "above"
				}
			]
		}, {
			page: "settings",
			location: "right",
			orientation: "vertical",
			widgets: [{
					name: "MediumAd",
					gravity: "above"
				}
			]
		}, {
			'page': 'dashboard',
			'location': 'account-card',
			'devices': ['tablet', 'desktop', 'phone'],
			'widgets': [{
					'name': 'SmartAd',
					'spaceId': 6
				},
			]
		}, {
			'page': 'dashboard',
			'location': 'account-group',
			'devices': ['tablet', 'desktop', 'phone'],
			'widgets': [{
					'name': 'SmartAd',
					'spaceId': 7
				},
			]
		}, {
			page: "approvals",
			location: "right",
			orientation: "vertical",
			widgets: [{
					name: "MediumAd",
					gravity: "above"
				}
			]
		}
	]
};
__exports__['default'] = themeJS;});
