const DELAY = 1000; // 1 second
const MAX_ATTEMPTS = 3;
const ID_TOKEN_REFRESH_INTERVAL_MS = 240000; // 4 minutes, as id_token TTL is 5 minutes.
const UPDATE_INFORMATION_MAX_ATTEMPTS = 5;
const UPDATE_INFORMATION_ATTEMPTS_INTERVAL_MS = 1000;

let currentAttempt = 1;
let loginHookWasAdded = false;
let logoutHookWasAdded = false;
let visitorHasLoggedOut = false;

const getPreviousGliaContext = function() {
  let previousGliaContext = {};

  if (window.getGliaContext && typeof window.getGliaContext === 'function') {
    previousGliaContext = window.getGliaContext();
  }

  return previousGliaContext;
};

const updateGliaContext = function({idToken, userId, accessToken}) {
  if (!idToken) {
    currentAttempt = 1;
    visitorHasLoggedOut = true;
  } else if (visitorHasLoggedOut) {
    visitorHasLoggedOut = false;
  }

  const previousGliaContext = getPreviousGliaContext();
  window.getGliaContext = () => ({...previousGliaContext, idToken, accessToken});
}

const installGlia = function(callback) {
  var gliaIntegrationScriptUrl = 'https://api.glia.com/salemove_integration.js';
  if (window.__GliaIntegration && window.__GliaIntegration.site_id) {
    try {
      if (window.__GliaIntegration.force_site_id) {
        gliaIntegrationScriptUrl = gliaIntegrationScriptUrl + "?site_id=" + window.__GliaIntegration.site_id;
      } else {
        window.top.origin; // if the domains are different then cross origin blocking will throw an exception and hence needs site_id appended.
      }
    } catch (e) {
      gliaIntegrationScriptUrl = gliaIntegrationScriptUrl + "?site_id=" + window.__GliaIntegration.site_id;
    }
  }
  var scriptElement = document.createElement('script');
  scriptElement.async = 1;
  scriptElement.src = gliaIntegrationScriptUrl;
  scriptElement.type = 'text/javascript';
  if (typeof(callback) === 'function') {
    scriptElement.addEventListener('load', callback);
    scriptElement.addEventListener('error', callback);
  }
  document.body.append(scriptElement);
};

const prepareCustomAttributes = function(user, account) {
  const replace = function(object){
    Object.keys(object).forEach(function(key){
      if (object[key]){
        typeof object[key] == 'object' ? replace(object[key]) : object[key]= String(object[key]);
      }
      else {
        object[key] = '';
      }
    });
  }
  const strUser = replace(user);
  const strAccount = replace(account);
  const attributes = Object.assign({}, user, account);
  delete attributes.external_id;
  return attributes;
}

const updateInformation = ({gliaApi, user, customAttributes}) => {
  gliaApi.updateInformation({
    name: `${user.first_name} ${user.last_name}`,
    customAttributesUpdateMethod: 'merge',
    customAttributes: customAttributes,
    externalId: user.external_id
  }).catch(function(error) {
    const log = [{
      level: 'warn',
      payload: `updateInformation call failed: ${JSON.stringify(error)}`
    }]

    sendLogs(log)
  });

  logCustomAttributes(customAttributes)
}

const tryUpdateInformation = ({gliaApi, user, customAttributes, initialVisitorId, attempt}) => {
  setTimeout(() => {
    const currentVisitorId = gliaApi.getVisitorId();

    // If visitor_id has not changed after 5 attempts, it probably means that authentication on Glia's side
    // has failed. If that's the case, there is no need to update the information for the same visitor_id again.
    if (attempt > UPDATE_INFORMATION_MAX_ATTEMPTS) return;

    if (initialVisitorId !== currentVisitorId) {
      updateInformation({gliaApi, user, customAttributes});
    } else {
      attempt += 1;
      tryUpdateInformation({gliaApi, user, customAttributes, initialVisitorId, attempt});
    }
  }, UPDATE_INFORMATION_ATTEMPTS_INTERVAL_MS)
}

const gSetVisitorInformation = function(user, account, isDirectIdAuthentication){
  const customAttributes = prepareCustomAttributes(user, account);

  sm.getApi({version: 'v1'}).then(function(gliaApi) {
    // Update information for current visitor_id instantly.
    updateInformation({gliaApi, user, customAttributes});

    if (isDirectIdAuthentication) {
      // In case Direct ID authentication was initiated (by returning id_token from getGliaContext function),
      // there will be a delay before visitor will be authenticated on Glia's side.
      // Reason for the delay is that we have JS timer setup in visitor-js-api which polls getGliaContext function
      // every 1 sec (at the time of writing). If a new id_token is detected, request to backend is sent and
      // id_token is verified. Only after visitor-js-api receives successful verification response, we consider
      // visitor authenticated.
      // On authentication, Glia's visitor_id will change.

      // We need to delay updating visitor information here until Glia's visitor_id changes.
      // Otherwise visitor information would not be updated for the authenticated visitor_id.
      const initialVisitorId = gliaApi.getVisitorId();
      const attempt = 1;

      tryUpdateInformation({gliaApi, user, customAttributes, initialVisitorId, attempt});
    }
  });
}

const installGliaForAuthenticatedUsers = function(onSuccess, onFailure) {
  if (!window.Ngam) {
    onFailure();

    return;
  }

  const loginFlow = Ngam.__container__.lookup('service:loginFlow');
  const loginController = Ngam.__container__.lookup('controller:login');
  const loginFlowReq = loginController && loginController.get('loginFlowRequirements');
  const applicationController = Ngam.__container__.lookup('controller:application');
  const notificationCenter = applicationController && applicationController.get('notificationCenter');
  const loginFlowSuccess = loginFlow && loginFlow.authenticatedStatus === 200;
  const loginFlowReqSuccess = loginFlowReq && loginFlowReq.authenticatedStatus === 200;
  const loginSucceeded = loginFlowSuccess || loginFlowReqSuccess;

  if (!loginSucceeded && !notificationCenter) {
    onFailure();

    return;
  }

  const keepIdTokenUpToDate = () => {
    setTimeout(function() {
      if (visitorHasLoggedOut) return;

      wedgeIntegrationController.get('store').sendPostRequest(JSON.stringify({
        formData: `routing_key=user_data`
      }), 'mobilews/form/GliaIntegration', function({data}) {
        const response = JSON.parse(data.forms[0]);
        const idToken = response.data.id_token;
        const accessToken = response.data.access_token
        const user = response.data.user

        if (idToken) {
          updateGliaContext({idToken, userId: user.auser_id, accessToken});

          keepIdTokenUpToDate();
        }

      }, function(errorResponse) {
        const log = [{level: 'warn', payload: `Error response from user_data endpoint: ${JSON.stringify(errorResponse)}`}]
        sendLogs(log)

        updateGliaContext({idToken: null, userId: null, accessToken: null});
      });

   }, ID_TOKEN_REFRESH_INTERVAL_MS);
  }

  const loginHook = userData => {
    const hasAlreadyLoggedIn = currentAttempt === MAX_ATTEMPTS; // Just in case loginHook ever fires more than once.

    if (hasAlreadyLoggedIn) {
      return;
    }

    if (userData.userId === 0) { // Secure Access Code workflow related check
      onFailure();

      return;
    }

    wedgeIntegrationController.get('store').sendPostRequest(JSON.stringify({
      formData: `routing_key=user_data`
    }), 'mobilews/form/GliaIntegration', function ({ data }) {
      const response = JSON.parse(data.forms[0]);
      const idToken = response.data.id_token;
      const user = response.data.user;
      const account = response.data.account;
      const isDirectIdAuthentication = Boolean(idToken);
      const accessToken = response.data.access_token

      if (isDirectIdAuthentication) {
        updateGliaContext({idToken, userId: user.auser_id, accessToken});

        keepIdTokenUpToDate();
      } else {
        const log = [{
          level: 'info',
          payload: 'Did not receive id_token from user_info endpoint',
          tags: {q2_user_id: user.auser_id}
        }]

        sendLogs(log)

        updateGliaContext({idToken: null, userId: null, accessToken: null});
      }

      const setup = () => {
        gSetVisitorInformation(user, account, isDirectIdAuthentication);

        onSuccess();
      }

      if (window.sm) {
        setup();

        return;
      }

      installGlia(function() {
        if (window.sm) {
          console.log('Glia is installed');

          setup();

          return;
        }

        onFailure();
      });
    }, (errorResponse) => {
      if (errorResponse.status !== 200) {
        const log = [{level: 'warn', payload: `Error response from user_data endpoint: ${JSON.stringify(errorResponse)}`}]
        sendLogs(log)

        updateGliaContext({idToken: null, userId: null});
      }

      onFailure();
    });
  }

  const logoutHook = () => {
    updateGliaContext({idToken: null, userId: null});
  }

  if (loginFlowSuccess && !visitorHasLoggedOut) {
    loginHook(loginFlow);
  } else if (loginFlowReqSuccess && !visitorHasLoggedOut) {
    loginHook(loginFlowReq);
  } else if (notificationCenter && !loginHookWasAdded) {
    const addLoginHook = () => {
      notificationCenter.on('POST_LOGIN', loginHook);
      loginHookWasAdded = true;
    };

    if (!window.sm) {
      installGlia(() => {
        if (window.sm) {
          console.log('Glia is installed');

          addLoginHook();
        }
      });

    }
    addLoginHook();
  }

  if (!logoutHookWasAdded) {
    notificationCenter.on('BEFORE_LOGOFF', logoutHook);
    logoutHookWasAdded = true;
  }

  return;
};

const onSuccessfulInstall = () => {
  currentAttempt = MAX_ATTEMPTS;
};
const onFailedInstall = () => {
  if (currentAttempt < MAX_ATTEMPTS) {
    currentAttempt++;
    tryInstallingGlia();
  } else {
    if (window.sm) {
      return;
    }

    installGlia(() => {
      if (window.sm) {
        console.log('Glia is installed');
      }
    });
  }
};

const tryInstallingGlia = () => {
  setTimeout(() => {
    installGliaForAuthenticatedUsers(onSuccessfulInstall, onFailedInstall);
  }, DELAY);
}

const logCustomAttributes = customAttributes => {
  const keysWithValue = Object.keys(Object.entries(customAttributes).reduce((a,[k,v]) => (v ? (a[k]=v, a) : a), {}))
  const keys = Object.keys(customAttributes)
  const keysNovalue = keys.filter(x => !keysWithValue.includes(x));

  const tags = {attributes: keysWithValue.toString(), blank_attributes: keysNovalue.toString(), q2_user_id: customAttributes.auser_id}

  const log = [{level: 'info', payload: 'updateInformation call detected (see keys in tags)', tags: tags}]
  // keys_with_value and keys_no_value .toString to avoid the list being pruned in kibana
  sendLogs(log)
}

const maxRetries = 5;
const retryInterval = 3000;
let retries = 0;

const sendLogs = logs => {
  if (logs == undefined || logs.length == 0) return;

  if (window.sm == undefined || sm.logger == undefined) {
    // In case Glia is not yet installed, we can retry it after 3 seconds.
    if (retries <= maxRetries) {
      setTimeout(() => {
        sendLogs(logs)
      }, retryInterval);

      retries += 1;
    }

    return;
  }

  retries = 0;

  sendToGlia(logs[0])
  sendLogs(logs.slice(1))
}


const sendToGlia = log => {
  const defaultTags = {
    glia_team: 'sudo',
    origin: 'q2_glia_integration'
  };

  tags = Object.assign(defaultTags, log.tags)

  if (window.sm && sm.logger !== undefined) {
    switch (log.level) {
      case 'warn':
        sm.logger.warn(log.payload, tags);
        break;
      case 'error':
        sm.logger.error(log.payload, tags);
        break;
      case 'info':
      default:
        sm.logger.info(log.payload, tags);
        break;
    }
  }
}

/**
 * The reason why it's done in such a manner is because we need to be able to tell
 * if the user is authenticated or not. It's possible only when Q2 is fully loaded
 * and when its Frontend successfully confirmed authentication status with its
 * Backend. Sometimes it takes more than one attempt for Q2 Frontend to get a
 * confirmation. Without it - we can't make authenticated requests to our Backend
 * extension
 *
 * There's no way to determine whether a different Frontend app which runs in parallel
 * and does its own logic has finished it via standard APIs we have access to.
 *
 * Q2 runs Ember app and it seems that it's also not possible to effectively hack into
 * app lifecycle and determine whether all redirects have happened, all hooks have been
 * executed and so on.
 *
 * But even we could do so - having a simpler approach with timeout and retry seems to
 * be more stable.
 */
if (document.readyState === 'complete') {
  tryInstallingGlia();
} else {
  window.addEventListener('load', tryInstallingGlia);
}
