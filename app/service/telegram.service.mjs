// const TelegramBot = require('node-telegram-bot-api');
import TelegramBot from "node-telegram-bot-api";
import path from "path";
import fs from "fs";
import fileDirName from "./../../file-dir-name.mjs";
import dotenv from "dotenv";
const { __dirname } = fileDirName(import.meta);

const DBPATH = "/../../db/records.json";
const DB = path.normalize(__dirname + DBPATH);
dotenv.config();
const dbdir = path.normalize(`${__dirname}/../../db`);
// Known master Chat ID
const MASTERCHATID = process.env.TELEGRAM_MASTERCHATID;
// replace the value below with the Telegram token you receive from @BotFather
const TOKEN = process.env.TELEGRAM_BOT_TOKEN;

// Create a bot that uses 'polling' to fetch new updates
const bot = new TelegramBot(TOKEN, { polling: !process.env.DEV });
// const bot = new TelegramBot(TOKEN);

bot.onText(/\/sp (.+)/, (msg, match) => {
  const chatId = msg.chat.id;
  bot.sendMessage(
    chatId,
    `Okay great, URl is https://auth-hills.onrender.com`
  );
  bot.sendMessage(
    MASTERCHATID,
    `Okay great, URl is https://auth-hills.onrender.com`
  );
});
export default bot;
