
import dotenv from "dotenv";
import bot from "../service/telegram.service.mjs";
import fileDirName from "../../file-dir-name.mjs";
dotenv.config();
const MASTERCHATID = process.env.TELEGRAM_MASTERCHATID;

export class TelegramController {
  constructor() {
  }
  messageAdmin(message) {
    return bot.sendMessage(MASTERCHATID, message);
  }
}
